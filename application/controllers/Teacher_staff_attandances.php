<?php
defined('BASEPATH') or exit('No direct script access allowed');

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Teacher_staff_attandances extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library(["form_validation", 'session']);
        $this->load->model(['teacher_staff_attandance_model']);
        $this->load->helper(['form', 'url']);
        if (!$this->session->userdata('status')) {
            $this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissible" role="alert">
            <div class="alert-message">
            Login terlebih dahulu!
            </div>
        </div>');
            redirect('auth/login');
        }
    }

    public function report()
    {
        if ($this->session->userdata('role') !== 'admin_absensi' && $this->session->userdata('role') !== 'Viewer' && $this->session->userdata('role') !== 'operator_absensi') {
            show_404();
        }
        if ($this->input->get('startDate') && $this->input->get('endDate')) {
            $attendances = $this->db->where('teacher_staff_attandances.date >=', $this->input->get('startDate'));
            $attendances = $this->db->where('teacher_staff_attandances.date <=', $this->input->get('endDate'));
        }

        $attendances = $this->db->order_by('teacher_staff_attandances.date', 'desc');
        $attendances = $this->teacher_staff_attandance_model->get_teacher_staff_attandances();

        $attendance_by_name = array();
        $dates = array();
        foreach ($attendances as $attendance) {
            $name = $attendance["teacher_staff_nama"];
            $position = $attendance["teacher_staff_jabatan"];
            $date = $attendance["date"];
            $status = $attendance["status_hadir"];
            $ket = $attendance["ket"];
            $attendance_by_name[$name][$date]["status"] = $status;
            $attendance_by_name[$name][$date]["ket"] = $ket;
            $attendance_by_name[$name]["position"] = $position;
            if (!in_array($date, $dates)) {
                $dates[] = $date;
            }
        }


        $data = [
            'title' => 'Absensi Guru / Staff',
            'dates' => $dates,
            'attendance_by_name' => $attendance_by_name
        ];

        $this->load->view('dashboard/teacher_staff_attandances/report', $data);
    }
    public function index()
    {
        if ($this->session->userdata('role') !== 'admin_absensi' && $this->session->userdata('role') !== 'Viewer' && $this->session->userdata('role') !== 'operator_absensi') {
            show_404();
        }
        if ($this->input->get('startDate') && $this->input->get('endDate')) {
            $attandances = $this->db->where('teacher_staff_attandances.date >=', $this->input->get('startDate'));
            $attandances = $this->db->where('teacher_staff_attandances.date <=', $this->input->get('endDate'));
        }

        $attandances = $this->db->order_by('teacher_staff_attandances.date', 'desc');
        $attandances = $this->teacher_staff_attandance_model->get_teacher_staff_attandances();

        $data = [
            'title' => 'Absensi Guru / Staff',
            'attandances' => $attandances
        ];

        $this->load->view('dashboard/teacher_staff_attandances/index', $data);
    }

    public function edit($id_teacher_staff_attandance)
    {
        if ($this->session->userdata('role') !== 'admin_absensi' && $this->session->userdata('role') !== 'Viewer' && $this->session->userdata('role') !== 'operator_absensi') {
            show_404();
        }
        $attandance = $this->db->join('teacher_staffs', 'teacher_staffs.id_teacher_staff = teacher_staff_attandances.id_teacher_staff');
        $attandance = $this->db->get_where('teacher_staff_attandances', array('id_teacher_staff_attandance' => $id_teacher_staff_attandance))->row_array();
        $data = [
            'title' => 'Absensi Siswa',
            'teacher_staff_attandance' => $attandance
        ];

        $this->load->view('dashboard/teacher_staff_attandances/edit', $data);
    }

    public function update()
    {
        if ($this->session->userdata('role') == 'viewer') {
            show_404();
        }

        if ($this->session->userdata('role') == 'operator_absensi' && $this->input->post('is_updated') == 1) {
            show_404();
        }

        $this->form_validation->set_rules('masuk', 'Masuk', 'required|trim');
        $this->form_validation->set_rules('keluar', 'Keluar', 'required|trim');
        $this->form_validation->set_rules('status_hadir', 'Status Hadir', 'required|trim');
        $this->form_validation->set_rules('ket', 'Keterangan', 'required|trim');

        if ($this->form_validation->run() == false) {
            $this->edit($this->input->post('id_teacher_staff_attandance'));
        } else {
            $this->teacher_staff_attandance_model->update($this->input->post('id_teacher_staff_attandance'), [
                'is_updated' => 1,
                'masuk' => $this->input->post('masuk'),
                'keluar' => $this->input->post('keluar'),
                'status_hadir' => $this->input->post('status_hadir'),
                'ket' => $this->input->post('ket'),
            ]);
            $this->session->set_flashdata('success', 'Kelas Berhasil Diperbarui!');
            redirect('teacher_staff_attandances');
        }
    }
}
