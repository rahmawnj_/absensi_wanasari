<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dashboard extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model(['user_model']);

        $this->load->helper(['form', 'url']);
        $this->load->library('form_validation');
        $this->load->model(['student_model', 'user_model', 'class_model']);

        if (!$this->session->userdata('status')) {
            $this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissible" role="alert">
            <div class="alert-message">
            Login terlebih dahulu!
            </div>
        </div>');
            redirect('auth/login');
        }
    }

    public function index()
    {
        if ($this->session->userdata('role') !== 'admin_absensi' && $this->session->userdata('role') !== 'Viewer' && $this->session->userdata('role') !== 'operator_absensi') {
            show_404();
        }
        $attendances = $this->db->query('SELECT date, COUNT(*) AS total
        FROM attendances
        WHERE status_hadir != "Alfa"
        GROUP BY date;
        ')->result_array();

        $dates = array_unique(array_merge(array_column($attendances, 'date')));

        // Mengurutkan tanggal secara ascending
        sort($dates);

        $data_student = array();

        // Inisialisasi categories
        $categories = array();

        // Mengisi categories dengan tanggal
        foreach ($dates as $date) {
            $categories[] = $date;
        }
        foreach ($dates as $date) {


            // Mengisi data untuk student
            $student_total = 0;
            foreach ($attendances as $attandance) {
                if ($attandance['date'] == $date) {
                    $student_total = $attandance['total'];
                    break;
                }
            }
            $data_student[] = $student_total;
        }

        $date_now = date('Y-m-d');
        $nowStudent = $this->db->get_where('attendances', array('date' => $date_now, 'status_hadir !=' => 'Alfa'))->result_array();

        $this->load->view('dashboard/index', [
            'title' => 'Dashboard',
            'nowStudent' => count($nowStudent),
            'users' => count($this->user_model->get_users()),
            'classes' => count($this->class_model->get_classes()),
            'students' => count($this->student_model->get_students()),
            'student_data' => json_encode($data_student),
            'categories' => json_encode($categories),
        ]);
    }
}
