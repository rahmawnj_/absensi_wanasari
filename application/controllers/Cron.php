<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Cron extends CI_Controller 
{
    /**
     * This is default constructor of the class
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model(['Teacher_staff_attandance_model','Teacher_staff_model', 'student_model']);
    }
    
    /**
     * This function is used to update the age of users automatically
     * This function is called by cron job once in a day at midnight 00:00
     */
    public function index()
    {    
        $date_now = date('Y-m-d');
        $day_now = date('l');

        $holiday_student = $this->db->get_where('holidays', ['waktu' => $date_now, 'type' => 'student'])->row_array();;
        if ((!$holiday_student && ($day_now !== 'Saturday')) && (!$holiday_student && ($day_now !== 'Sunday'))) {
            $students = $this->student_model->get_students();
            foreach($students as $student) {
                $attandance = $this->db->get_where('attendances', array('date' => $date_now, 'attendances.id_student' => $student['id_student']))->row_array();
                if (!$attandance) {
                    $this->db->insert('attendances', [
                        'id_student' => $student['id_student'],
                        'masuk' => 0,
                        'waktu_masuk' => "-",
                        'keluar' => 0,
                        'waktu_keluar' => "-",
                        'status_hadir' => 'Alfa',
                        'ket'  => 'Alfa',
                        'date' => $date_now,
                    ]);
                }
            }
        }

        
    }
}
?>