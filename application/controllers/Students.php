<?php
defined('BASEPATH') or exit('No direct script access allowed');

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;


class Students extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library("form_validation");
        $this->load->model(['student_model', 'class_model']);
        $this->load->helper(['form', 'url']);
        if (!$this->session->userdata('status')) {
            $this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissible" role="alert">
            <div class="alert-message">
            Login terlebih dahulu!
            </div>
        </div>');
            redirect('auth/login');
        }
    }

    public function template()
    {
        $spreadsheet = new PhpOffice\PhpSpreadsheet\Spreadsheet();

        $sheet = $spreadsheet->getActiveSheet();
        // set Header
        $sheet->SetCellValue('A1', 'nama')->getColumnDimension('A')->setAutoSize(true);
        $sheet->SetCellValue('B1', 'nisn')->getColumnDimension('B')->setAutoSize(true);
        $sheet->SetCellValue('C1', 'rfid')->getColumnDimension('C')->setAutoSize(true);
        $sheet->SetCellValue('D1', 'jenis_kelamin')->getColumnDimension('D')->setAutoSize(true);

        $sheet->SetCellValue('A2', 'Putra')->getColumnDimension('A')->setAutoSize(true);
        $sheet->SetCellValue('B2', '11111111')->getColumnDimension('B')->setAutoSize(true);
        $sheet->SetCellValue('C2', '1111111')->getColumnDimension('C')->setAutoSize(true);
        $sheet->SetCellValue('D2', 'Laki-laki')->getColumnDimension('D')->setAutoSize(true);

        $sheet->SetCellValue('A3', 'Putri')->getColumnDimension('A')->setAutoSize(true);
        $sheet->SetCellValue('B3', '222222222')->getColumnDimension('B')->setAutoSize(true);
        $sheet->SetCellValue('C3', '222222')->getColumnDimension('C')->setAutoSize(true);
        $sheet->SetCellValue('D3', 'Perempuan')->getColumnDimension('D')->setAutoSize(true);

        $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
        $filename = 'example-siswa';

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        header('Cache-Control: max-age=0');

        $writer->save('php://output'); // download file 

    }


    public function export_excel()
    {
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        // set Header
        $sheet->SetCellValue('A1', 'NISN')->getColumnDimension('A')->setAutoSize(true);
        $sheet->SetCellValue('B1', 'Nama')->getColumnDimension('B')->setAutoSize(true);
        $sheet->SetCellValue('C1', 'Jenis Kelamin')->getColumnDimension('C')->setAutoSize(true);
        $sheet->SetCellValue('D1', 'RFID')->getColumnDimension('D')->setAutoSize(true);
		
        // set Row
        $students = $this->student_model->get_students();
        $rowCount = 2;
        foreach ($students as $student) {
            $sheet->SetCellValue('A' . $rowCount, $student['nisn'])->getColumnDimension('A')->setAutoSize(true);
            $sheet->SetCellValue('B' . $rowCount, $student['nama'])->getColumnDimension('B')->setAutoSize(true);
            $sheet->SetCellValue('C' . $rowCount, $student['jenis_kelamin'])->getColumnDimension('C')->setAutoSize(true);
            $sheet->SetCellValue('D' . $rowCount, $student['rfid'])->getColumnDimension('D')->setAutoSize(true);
            $rowCount++;
        }

        $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);

        $filename = date('D-M-Y');

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        header('Cache-Control: max-age=0');

        $writer->save('php://output'); // download file 

    }

    // file upload functionality
    public function upload()
    {
        $data = array();
        // Load form validation library
        $this->load->library('form_validation');
        $this->form_validation->set_rules('fileURL', 'Upload File', 'callback_checkFileValidation');
        if ($this->form_validation->run() == false) {

            $this->load->view('spreadsheet/index', $data);
        } else {
            // If file uploaded
            if (!empty($_FILES['fileURL']['name'])) {
                // get file extension
                $extension = pathinfo($_FILES['fileURL']['name'], PATHINFO_EXTENSION);

                if ($extension == 'csv') {
                    $reader = new \PhpOffice\PhpSpreadsheet\Reader\Csv();
                } elseif ($extension == 'xlsx') {
                    $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
                } else {
                    $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xls();
                }
                // file path
                $spreadsheet = $reader->load($_FILES['fileURL']['tmp_name']);
                $allDataInSheet = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);

                // array Count
                $arrayCount = count($allDataInSheet);
                $flag = 0;
                $createArray = ['nama', 'nisn', 'rfid', 'jenis_kelamin'];
                $makeArray = ['nama' => 'nama', 'nisn' => 'nisn', 'rfid' => 'rfid', 'jenis_kelamin' => 'jenis_kelamin'];
                $SheetDataKey = [];
                foreach ($allDataInSheet as $dataInSheet) {
                    foreach ($dataInSheet as $key => $value) {
                        if (in_array(trim($value), $createArray)) {
                            $value = preg_replace('/\s+/', '', $value);
                            $SheetDataKey[trim($value)] = $key;
                        }
                    }
                }

                $dataDiff = array_diff_key($makeArray, $SheetDataKey);
                if (empty($dataDiff)) {
                    $flag = 1;
                }

                // match excel sheet column
                if ($flag == 1) {
                    for ($i = 2; $i <= $arrayCount; $i++) {
                        $nama = $SheetDataKey['nama'];
                        $nisn = $SheetDataKey['nisn'];
                        $rfid = $SheetDataKey['rfid'];
                        $jenis_kelamin = $SheetDataKey['jenis_kelamin'];


                        $nama = filter_var(trim($allDataInSheet[$i][$nama]), FILTER_SANITIZE_STRING);
                        $nisn = filter_var(trim($allDataInSheet[$i][$nisn]), FILTER_SANITIZE_STRING);
                        $rfid = filter_var(trim($allDataInSheet[$i][$rfid]), FILTER_SANITIZE_STRING);
                        $jenis_kelamin = filter_var(trim($allDataInSheet[$i][$jenis_kelamin]), FILTER_SANITIZE_STRING);
                        $id_class = filter_var(trim(0), FILTER_SANITIZE_STRING);
                        $deleted = filter_var(trim(0), FILTER_SANITIZE_STRING);
                        $fetchData[] = ['id_class' => $id_class, 'deleted' => $deleted, 'nama' => $nama, 'nisn' => $nisn, 'rfid' => $rfid,'jenis_kelamin' => $jenis_kelamin];
                    }
                    $data['dataInfo'] = $fetchData;
                    $this->student_model->setBatchImport($fetchData);
                    $this->student_model->importData();
                } else {
                    echo "Please import correct file, did not match excel sheet column";
                }
                $this->session->set_flashdata('success', 'Siswa Berhasil Ditambahkan!');
                redirect('dashboard/students');
            }
        }
    }

    // checkFileValidation
    public function checkFileValidation($string)
    {
        $file_mimes = array(
            'text/x-comma-separated-values',
            'text/comma-separated-values',
            'application/octet-stream',
            'application/vnd.ms-excel',
            'application/x-csv',
            'text/x-csv',
            'text/csv',
            'application/csv',
            'application/excel',
            'application/vnd.msexcel',
            'text/plain',
            'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
        );
        if (isset($_FILES['fileURL']['name'])) {
            $arr_file = explode('.', $_FILES['fileURL']['name']);
            $extension = end($arr_file);
            if (($extension == 'xlsx' || $extension == 'xls' || $extension == 'csv') && in_array($_FILES['fileURL']['type'], $file_mimes)) {
                return true;
            } else {
                $this->form_validation->set_message('checkFileValidation', 'Please choose correct file.');
                return false;
            }
        } else {
            $this->form_validation->set_message('checkFileValidation', 'Please choose a file.');
            return false;
        }
    }

    public function index()
    {
        if ($this->session->userdata('role') !== 'admin_absensi' && $this->session->userdata('role') !== 'Viewer' && $this->session->userdata('role') !== 'operator_absensi') {
            show_404();
        }
        $data = [
            'title' => 'Siswa',
            'students' => $this->student_model->get_students()
        ];

        $this->load->view('dashboard/students/index', $data);
    }

    public function create()
    {
        if ($this->session->userdata('role') !== 'admin_absensi') {
            show_404();
        }
        $data = [
            'title' => 'Siswa',
            'classes' => $this->class_model->get_classes()
        ];

        $this->load->view('dashboard/students/create', $data);
    }

    public function store()
    {

        if ($this->session->userdata('role') !== 'admin_absensi') {
            show_404();
        }

        $this->form_validation->set_rules('class', 'Class', 'required|trim');
        $this->form_validation->set_rules('nama', 'Name', 'required|trim');
        $this->form_validation->set_rules('nisn', 'NISN', 'required|trim|is_unique[students.nisn]');
        $this->form_validation->set_rules('rfid', 'RFID', 'required|trim|is_unique[students.rfid]');
        $this->form_validation->set_rules('jenis_kelamin', 'Gender', 'required|trim');


        if ($this->form_validation->run() == false) {
            $this->create();
        } else {


            $config['upload_path']          = './assets/img/uploads/students';
            $config['allowed_types']        = 'gif|jpg|png|jpeg|jfif';
            $config['max_size']             = 10000;
            $config['max_width']            = 10000;
            $config['max_height']           = 10000;

            $this->load->library('upload', $config);
            $this->upload->do_upload('foto');

            $this->student_model->insert([
                'id_class' => $this->input->post('class'),
                'nama' => $this->input->post('nama'),
                'nisn' => $this->input->post('nisn'),
                'rfid' => $this->input->post('rfid'),
                'jenis_kelamin' => $this->input->post('jenis_kelamin'),
                'deleted' => 0,
                'foto' => 'students/' . $this->upload->data('file_name'),
                'created_at' => date('Y-m-d H:i:s')
            ]);

            $this->session->set_flashdata('success', 'Siswa Berhasil Ditambahkan!');
            redirect('dashboard/students');
        }
    }

    public function view($id_student)
    {
        if ($this->session->userdata('role') !== 'admin_absensi') {
            show_404();
        }
        $data = [
            'title' => 'Siswa',
            'student' => $this->student_model->get_student('id_student', $id_student)
        ];

        $this->load->view('dashboard/students/view', $data);
    }

    public function edit($id_student)
    {
        if ($this->session->userdata('role') !== 'admin_absensi') {
            show_404();
        }
        $data = [
            'title' => 'Siswa',
            'classes' => $this->class_model->get_classes(),
            'student' => $this->student_model->get_student('id_student', $id_student)
        ];

        $this->load->view('dashboard/students/edit', $data);
    }

    public function update()
    {
        if ($this->session->userdata('role') !== 'admin_absensi') {
            show_404();
        }
        $student = $this->student_model->get_student('id_student', $this->input->post('id_student'));

        if ($this->input->post('nisn') == $student['nisn']) {
            $nis_rules = 'required|trim';
        } else {
            $nis_rules = 'required|trim|is_unique[students.nisn]';
        }
        if ($this->input->post('rfid') == $student['rfid']) {
            $rfid_rules = 'required|trim';
        } else {
            $rfid_rules = 'required|trim|is_unique[students.rfid]';
        }


        $this->form_validation->set_rules('class', 'Class', 'required|trim');
        $this->form_validation->set_rules('nama', 'Name', 'required|trim');
        $this->form_validation->set_rules('nisn', 'NISN', $nis_rules);
        $this->form_validation->set_rules('rfid', 'RFID', $rfid_rules);
        $this->form_validation->set_rules('jenis_kelamin', 'Gender', 'required|trim');


        if ($this->form_validation->run() == false) {
            $this->edit($this->input->post('id_student'));
        } else {
            
            $config['upload_path']          = './assets/img/uploads/students';
            $config['allowed_types']        = 'gif|jpg|png|jpeg|jfif';
            $config['max_size']             = 10000;
            $config['max_width']            = 10000;
            $config['max_height']           = 10000;

            $this->load->library('upload', $config);
            $this->upload->do_upload('foto');

            if ($this->upload->data('file_name') == '') {
                $foto = $student['foto'];
            } else {
                unlink('./assets/img/uploads/' . $student['foto']);
                $foto = 'students/' . $this->upload->data('file_name');
            }
          

            $this->student_model->update($this->input->post('id_student'), [
                'id_class' => $this->input->post('class'),
                'nama' => $this->input->post('nama'),
                'nisn' => $this->input->post('nisn'),
                'rfid' => $this->input->post('rfid'),
                'jenis_kelamin' => $this->input->post('jenis_kelamin'),
                'foto' => $foto,
                'updated_at' => date('Y-m-d H:i:s')
            ]);

            $this->session->set_flashdata('success', 'Siswa Berhasil Diperbarui!');
            redirect('dashboard/students');
        }
    }

    public function delete($id_student)
    {
        if ($this->session->userdata('role') !== 'admin_absensi') {
            show_404();
        }
        $student = $this->student_model->get_student('id_student', $id_student);

        $this->student_model->delete($id_student);
        unlink('./assets/img/uploads/' . $student['foto']);
        $this->session->set_flashdata('success', 'Siswa Berhasil Dihapus!');
        redirect('dashboard/students');
    }
}
