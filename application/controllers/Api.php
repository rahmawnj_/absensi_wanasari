<?php
defined('BASEPATH') or exit('No direct script access allowed');

use chriskacerguis\RestServer\RestController;

class Api extends RestController
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library("form_validation");
        $this->load->model(['student_model', 'Teacher_staff_model', 'teacher_staff_attandance_model', 'holiday_model', 'student_attandance_model', 'attandance_device_model', 'operational_time_model', 'setting_model']);
        $this->load->helper(['form', 'url']);
    }

    public function checkrfid_post()
    {
        $student = $this->student_model->get_student('rfid', $this->input->post('rfid'));
        if ($student) {
            $data = [
                'id' => $student['id_student'],
                'nama' => $student['nama'],
                'rfid' => $student['rfid'],
                'foto' => '/assets/img/uploads/' . $student['foto'],
            ];
            $this->response(['status' => 'success', 'data' => $data]);
        } else {

            $this->response(['status' => 'error', 'data' => 'data-tidak-ditemukan']);
        }
    }

    public function absensi_post()
    {
       
        $student = $this->student_model->get_student('rfid', $this->input->post('rfid'));

        $type = null;
        if ($student) {
            $type = 'student';
            $typeUser = 'siswa';
        }
        if ($type) {
            $date_now = date('Y-m-d');
            $day_now = date('l');

            $holiday = $this->db->get_where('holidays', ['waktu' => $date_now, 'type' => $type])->row_array();
            $weekly_holiday = $this->db->get('weekly_holidays')->result_array();
            $weekend_attendance = $this->db->get_where('settings', array('name' => 'weekend_attendance'))->row_array();

            if (!$holiday && !in_array($day_now, array_column($weekly_holiday, 'hari'))) {

                $operational_time = $this->operational_time_model->get_operational_time($type);

                $masuk = explode('-', $operational_time['waktu_masuk']);
                $keluar = explode('-', $operational_time['waktu_keluar']);
                $masuk_awal = $masuk[0];
                $masuk_akhir = $masuk[1];
                $keluar_awal = $keluar[0];
                $keluar_akhir = $keluar[1];
                $telat = $operational_time['telat'];
                $time_now = date("H:i");

                if ($time_now < $masuk_akhir && $time_now > $masuk_awal) {
                    if ($type == 'student') {
                        $user = $student;
                        $attandance = $this->db->get_where('attendances', array('attendances.date' => $date_now, 'id_student' => $student['id_student']))->row_array();
                    }

                    if (!$attandance) {
                        if ($time_now < $telat) {
                            $status_hadir = 'Hadir - Tepat waktu';
                        } else {
                            $status_hadir = 'Hadir - Telat';
                        }
                        $ket = 'berhasil masuk';
                        if ($type == 'student') {
                            $this->db->insert('attendances', [
                                'image' => $this->input->post('image'),
                                'id_student' => $student['id_student'],
                                'masuk' => 1,
                                'waktu_masuk' => $time_now,
                                'keluar' => 0,
                                'status_hadir' => $status_hadir,
                                'ket'  => $ket,
                                'date' => $date_now,
                            ]);
                        }
                        $this->response(['ket' => $ket, 'status' => 'success', 'type' => $typeUser, $typeUser => $user, 'waktu' => date('H:i:s d/m/Y')]);
                    } else {
                        $this->response(['ket' => 'Sudah Absen', 'status' => 'success', 'type' => $typeUser, $typeUser => $user, 'waktu' => '-']);
                    }
                } else if ($time_now > $keluar_awal && $time_now < $keluar_akhir) {
                    $user = $student;
                    $attandance = $this->db->get_where('attendances', array('attendances.date' => $date_now, 'id_student' => $student['id_student']))->row_array();

                    if ($attandance) {
                        $ket = 'pulang';
                        if ($type == 'student') {
                            $this->db->update('attendances', [
                                'keluar' => 1,
                                'waktu_keluar' => $time_now,
                            ], ['id_attendance' => $attandance['id_attendance']]);
                        }
                        $this->response(['ket' => $ket, 'status' => 'success', 'type' => $typeUser, $typeUser => $user, 'waktu' => date('H:i:s d/m/Y')]);
                    } else {
                        $this->response(['ket' => 'Tidak absen masuk', 'status' => 'error']);
                    }
                } else {
                    $this->response(['ket' => 'Error waktu operasional', 'status' => 'error']);
                }
            } else {
                if ($weekend_attendance['value'] == 'on') {
                    $operational_time = $this->operational_time_model->get_operational_time($type);
                    $masuk = explode('-', $operational_time['waktu_masuk']);
                    $keluar = explode('-', $operational_time['waktu_keluar']);
                    $masuk_awal = $masuk[0];
                    $masuk_akhir = $masuk[1];
                    $keluar_awal = $keluar[0];
                    $keluar_akhir = $keluar[1];
                    $telat = $operational_time['telat'];
                    $time_now = date("H:i");
                    // $time_now = "07:30";
                    if ($time_now < $masuk_akhir && $time_now > $masuk_awal) {
                        if ($type == 'student') {
                            $user = $student;
                            $attandance = $this->db->get_where('attendances', array('attendances.date' => $date_now, 'id_student' => $student['id_student']))->row_array();
                        }
                        if (!$attandance) {
                            if ($time_now < $telat) {
                                $status_hadir = 'Hadir - Tepat waktu';
                            } else {
                                $status_hadir = 'Hadir - Telat';
                            }
                            $ket = 'berhasil masuk';
                            if ($type == 'student') {
                                $this->db->insert('attendances', [
                                    'image' => $this->input->post('image'),
                                    'id_student' => $student['id_student'],
                                    'masuk' => 1,
                                    'waktu_masuk' => $time_now,
                                    'keluar' => 0,
                                    'status_hadir' => 'Hadir',
                                    'ket'  => $ket,
                                    'date' => $date_now,
                                ]);
                            }
                            $this->response(['ket' => $ket, 'status' => 'success', 'type' => $typeUser, $typeUser => $user, 'waktu' => date('H:i:s d/m/Y')]);
                        } else {
                            $this->response(['ket' => 'Sudah Absen', 'status' => 'error']);
                        }
                    } else if ($time_now > $keluar_awal && $time_now < $keluar_akhir) {
                        if ($type == 'student') {
                            $user = $student;
                            $attandance = $this->db->get_where('attendances', array('attendances.date' => $date_now, 'id_student' => $student['id_student']))->row_array();
                        }
                        if ($attandance) {
                            $ket = 'pulang';
                            if ($type == 'student') {
                                $this->db->update('attendances', [
                                    'keluar' => 1,
                                    'waktu_keluar' => $time_now,
                                ], ['id_attendance' => $attandance['id_attendance']]);
                            }
                            $this->response(['ket' => $ket, 'status' => 'success', 'type' => $typeUser, $typeUser => $user, 'waktu' => date('H:i:s d/m/Y')]);
                        } else {
                            $this->response(['ket' => 'Absensi Gagal', 'status' => 'error']);
                        }
                    } else {
                        $this->response(['ket' => 'Error waktu operasional', 'status' => 'error']);
                    }
                } else {
                    $this->response(['ket' => 'Hari Libur', 'status' => 'error']);
                }
            }
        } else {
            $this->response(['ket' => 'rfid tidak ditemukan', 'status' => 'error']);
        }
    }

    public function datasiswa_get()
    {
        $students = $this->db->join('classes', 'classes.id_class = students.id_class')->where(['students.deleted' => 0])->get('students')->result_array();

        $datastudents = [];
        foreach ($students as $key => $student) {
            $datastudents[] = [
                'id' => $student['id_student'],
                'nama' => $student['nama'],
                'kelas' => $student['kelas'],
                'rfid' => $student['rfid'],
                'jenis_kelamin' => $student['jenis_kelamin'],
                'nisn' => $student['nisn'],
                'foto' =>  '/assets/img/uploads/' . $student['foto'],
            ];
        }

        $this->response(['status' => 'success', 'data' => $datastudents]);
    }

    public function send_student_wa($siswa, $waktu, $tanggal, $day, $ket)
    {
        $message_data = $this->setting_model->get_setting('name', 'whatsapp_message_student');
        // convert message
        $pesans = explode(' ', $message_data['value']);
        $message = '';
        foreach ($pesans as $pesan) {
            if (strpos($pesan, '$') !== false) {
                if ($pesan == '$nama') {
                    $message .= ' ' . $siswa['nama'];
                } else if ($pesan == '$kelas') {
                    $message .= ' ' . $siswa['kelas'];
                } else if ($pesan == '$waktu') {
                    $message .= ' ' . $waktu;
                } else if ($pesan == '$keterangan') {
                    $message .= ' ' . $ket;
                } else if ($pesan == '$tanggal') {
                    $message .= ' ' . $tanggal;
                } else if ($pesan == '$hari') {
                    $message .= ' ' . $day;
                } else {
                    $message .= ' ' . $pesan;
                }
            } else {
                $message .= ' ' . $pesan;
            }
        }
        $message = trim($message);
        if ($siswa['no_hp_ortu'] && (strlen($siswa['no_hp_ortu']) > 0 && $siswa['no_hp_ortu'][0] !== "0")) {

            $data = array(
                "nama" => $siswa['nama'],
                "telp" => '62' . $siswa['no_hp_ortu'],
                "message" => $message
            );

            $options = array(
                'http' => array(
                    'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
                    'method'  => 'POST',
                    'content' => http_build_query($data)
                )
            );

            $url = $this->setting_model->get_setting('name', 'wabot')['value'];
            // $url = 'http://wabot.tytomulyono.com/api/insert';
            $context  = stream_context_create($options);

            return file_get_contents($url, false, $context);
        }
    }

    public function send_teacherstaff_wa($teacher_staff, $waktu, $tanggal, $day, $ket)
    {
        $message_data = $this->setting_model->get_setting('name', 'whatsapp_message_teacherstaff');
        // convert message
        $pesans = explode(' ', $message_data['value']);
        $message = '';
        foreach ($pesans as $pesan) {
            if (strpos($pesan, '$') !== false) {
                if ($pesan == '$nama') {
                    $message .= ' ' . $teacher_staff['nama'];
                } else if ($pesan == '$jabatan') {
                    $message .= ' ' . $teacher_staff['jabatan'];
                } else if ($pesan == '$waktu') {
                    $message .= ' ' . $waktu;
                } else if ($pesan == '$keterangan') {
                    $message .= ' ' . $ket;
                } else if ($pesan == '$tanggal') {
                    $message .= ' ' . $tanggal;
                } else if ($pesan == '$hari') {
                    $message .= ' ' . $day;
                } else {
                    $message .= ' ' . $pesan;
                }
            } else {
                $message .= ' ' . $pesan;
            }
        }
        $message = trim($message);


        if (strlen($teacher_staff['no_hp']) > 0 && $teacher_staff['no_hp'][0] !== "0") {

            $data = array(
                "nama" => $teacher_staff['nama'],
                "telp" => '62' . $teacher_staff['no_hp'],
                "message" => $message
            );

            $options = array(
                'http' => array(
                    'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
                    'method'  => 'POST',
                    'content' => http_build_query($data)
                )
            );

            $url = $this->setting_model->get_setting('name', 'wabot')['value'];
            // $url = 'http://wabot.tytomulyono.com/api/insert';
            $context  = stream_context_create($options);

            return file_get_contents($url, false, $context);
        }
    }

    public function last_log_get($id = null)
    {
        $device = $this->db->get_where('attandance_devices', ['device' => $id])->row_array();
        if ($device) {
            $rfid = $device['rfid'];

            $student = $this->db->join('classes', 'classes.id_class = students.id_class')->where(['students.deleted' => 0])->get_where('students', ['rfid' => $rfid])->row_array();
            $teacher_staff = $this->db->where(['teacher_staffs.deleted' => 0])->get_where('teacher_staffs', ['rfid' => $rfid])->row_array();
            $type = null;
            if ($student) {
                $type = 'student';
                $data = $student;
            } else if ($teacher_staff) {
                $type = 'teacher_staff';
                $data = $teacher_staff;
            }

            $this->response(['msg' => 'success', 'type' => $type, 'data' => $data, 'device' => $device]);
        } else {
            $this->response(['msg' => 'success', 'logs' => 'id-tidak-terdaftar', 'data_user' => '']);
        }
    }
}
