<?php $this->load->view('layout/head') ?>

<div class="wrapper">
    <?php $this->load->view('layout/sidebar') ?>
    <div class="main">
        <?php $this->load->view('layout/header') ?>

        <main class="content">
            <div class="container-fluid p-0">
                <div class="row removable">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header">
                                <h5 class="card-title mb-0"><?= $title ?></h5>
                            </div>
                            <div class="card-body">
                                <?= $this->session->flashdata('message'); ?>

                                <?php echo form_open_multipart('teacher_staffs/store'); ?>
                                <div class="img-preview d-flex">
                                    <img src="<?= base_url('assets/img/default_user.jfif') ?>" id="gmbr" class="img-fluid img-thumbnail mx-auto d-block text-center" style="height:200px" alt="">
                                </div>
                                <div class="form-group">
                                    <label for="foto">Foto</label>
                                    <input autocomplete="off" type="file" id="gambar" class="form-control" size="20" name="foto" id="foto" aria-describedby="foto" required placeholder="Masukan foto">
                                    <span class="text-danger">
                                        <?= form_error('foto') ?>
                                    </span>
                                </div>

                                <div class="form-group">
                                    <label for="nama">Nama</label>
                                    <input autocomplete="off" type="text" class="form-control" value="<?= set_value('nama') ?>" name="nama" id="nama" aria-describedby="nama" placeholder="Masukan Nama" required>
                                    <span class="text-danger">
                                        <?= form_error('nama') ?>
                                    </span>
                                </div>

                                <div class="form-group">
                                    <label for="rfid">RFID</label>

                                    <input autocomplete="off" type="text" class="form-control" name="rfid" value="<?= set_value('rfid') ?>" id="rfid" aria-describedby="rfid" placeholder="Masukan RFID" required>
                                    <span class="text-danger">
                                        <?= form_error('rfid') ?>
                                    </span>
                                </div>

                                <div class="form-group">
                                    <label for="jabatan">jabatan</label>
                                    <select name="jabatan" class="form-control" id="jabatan" required>
                                        <option selected disabled>-- Pilih jabatan --</option>
                                        <option value="Guru">Guru</option>
                                        <option value="Staff">Staff</option>
                                    </select>
                                    <span class="text-danger">
                                        <?= form_error('jabatan') ?>
                                    </span>
                                </div>

                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                </div>
                                <?= form_close() ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>

        <?php $this->load->view('layout/footer') ?>
    </div>
</div>

<?php $this->load->view('layout/foot') ?>