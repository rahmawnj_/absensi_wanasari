-- phpMyAdmin SQL Dump
-- version 4.9.11
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Apr 07, 2023 at 09:24 AM
-- Server version: 10.3.38-MariaDB-cll-lve
-- PHP Version: 7.4.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `absensi3_absensi`
--

-- --------------------------------------------------------

--
-- Table structure for table `attandance_devices`
--

CREATE TABLE `attandance_devices` (
  `id_attandance_device` int(11) NOT NULL,
  `device` varchar(100) NOT NULL,
  `lokasi` varchar(100) NOT NULL,
  `gambar` varchar(100) NOT NULL,
  `rfid` varchar(50) NOT NULL,
  `time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `deleted` int(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `attandance_devices`
--

INSERT INTO `attandance_devices` (`id_attandance_device`, `device`, `lokasi`, `gambar`, `rfid`, `time`, `deleted`, `created_at`, `updated_at`) VALUES
(1, '1', 'tg 1', 'attandance_devices/Screenshot_(5)2.png', '5751AC7BD1', '2023-03-23 01:04:31', 0, '2023-03-23 01:04:31', '2023-02-13 04:04:29'),
(2, '', '', '', '', '2023-03-09 08:05:36', 1, '2023-02-24 00:31:05', '2023-02-17 01:54:11'),
(3, '', '90', 'attandance_devices/profile.png', '', '2023-03-09 08:05:36', 1, '2023-02-24 00:31:00', '2023-02-24 00:30:56');

-- --------------------------------------------------------

--
-- Table structure for table `attendances`
--

CREATE TABLE `attendances` (
  `id_attendance` int(11) NOT NULL,
  `id_student` int(11) NOT NULL,
  `image` text NOT NULL,
  `masuk` int(11) NOT NULL,
  `waktu_masuk` varchar(50) NOT NULL,
  `keluar` int(11) NOT NULL,
  `waktu_keluar` varchar(50) NOT NULL,
  `status_hadir` varchar(50) NOT NULL,
  `ket` varchar(50) NOT NULL,
  `date` date NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `is_updated` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `classes`
--

CREATE TABLE `classes` (
  `id_class` int(11) NOT NULL,
  `kelas` varchar(50) NOT NULL,
  `deleted` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `classes`
--

INSERT INTO `classes` (`id_class`, `kelas`, `deleted`, `created_at`, `updated_at`) VALUES
(0, '57984543', 1, '2022-11-08 07:25:14', NULL),
(2, 'Grade 11', 0, '2023-03-03 03:43:30', '2023-04-03 06:17:19'),
(3, 'Grade 11 MIA', 1, '2023-03-03 03:43:42', NULL),
(4, 'Grade 11 IIS', 1, '2023-03-03 03:43:52', NULL),
(5, 'Grade 12 MIA', 1, '2023-03-03 03:44:00', NULL),
(6, 'Grade 12 IIS', 1, '2023-03-03 03:44:10', NULL),
(7, '12', 0, '2023-04-04 21:09:02', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `holidays`
--

CREATE TABLE `holidays` (
  `id_holiday` int(11) NOT NULL,
  `name` varchar(20) NOT NULL,
  `waktu` date NOT NULL,
  `type` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `operational_times`
--

CREATE TABLE `operational_times` (
  `id_operational_time` int(11) NOT NULL,
  `waktu_masuk` varchar(20) NOT NULL,
  `telat` varchar(20) NOT NULL,
  `waktu_keluar` varchar(20) NOT NULL,
  `type` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `operational_times`
--

INSERT INTO `operational_times` (`id_operational_time`, `waktu_masuk`, `telat`, `waktu_keluar`, `type`) VALUES
(1, '14:00-16:00', '15:59', '17:00-18:00', 'student');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id_role` int(11) NOT NULL,
  `role` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id_role`, `role`) VALUES
(3, 'admin_absensi'),
(5, 'operator_absensi');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id_setting` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `value` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id_setting`, `name`, `value`) VALUES
(3, 'school_name', 'SMA 1 Wanasari'),
(8, 'weekend_attendance', 'on');

-- --------------------------------------------------------

--
-- Table structure for table `students`
--

CREATE TABLE `students` (
  `id_student` int(11) NOT NULL,
  `id_class` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `nisn` varchar(50) NOT NULL,
  `saldo` varchar(255) NOT NULL,
  `rfid` varchar(50) NOT NULL,
  `jenis_kelamin` varchar(10) NOT NULL,
  `pin` varchar(100) NOT NULL,
  `foto` varchar(255) NOT NULL,
  `deleted` int(11) NOT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `students`
--

INSERT INTO `students` (`id_student`, `id_class`, `nama`, `nisn`, `saldo`, `rfid`, `jenis_kelamin`, `pin`, `foto`, `deleted`, `updated_at`, `created_at`) VALUES
(1, 1, 'tes', '', 'Fw==', '', 'Perempuan', '$2y$10$todcoEBtZ1GPSzYs27POIOP8lFvHKyRpta2O8v9Kre2MPLUt2JaGu', 'students/IMG20230212155809_-_Ade_Akmaludin.jpg', 1, NULL, '2023-02-22 08:46:53'),
(2, 2, 'Rimiku Santalum Sativa', '', 'KUBF9HS0', '', 'Perempuan', '$2y$10$BTcCtKKNHsaCwfrSFQJ5l.CPbvNbXCAoy7en8uonN9KTKkl7jxHKq', 'students/graduated_-_Copy.png', 1, '2023-03-07 02:46:23', '2023-03-03 06:38:26'),
(3, 2, 'Almandiena Az-Zahra', '', 'oA==', '', 'Perempuan', '$2y$10$NS4TPggxn3/zWMWZvseuKepaoGAj0yBVPYV8TxZyWEYEJ7piYSv3G', 'students/graduated_-_Copy1.png', 1, '2023-03-06 03:53:27', '2023-03-03 06:41:54'),
(4, 3, 'DARRYL SELMA HANIFA SETIADI', '', 'PA==', '', 'Perempuan', '$2y$10$y/kq9tOH2c2cQs/XDojOneYiXlrZi2II3FQWKF9hS8ckJENb.zSn6', 'students/graduated_-_Copy3.png', 1, '2023-03-06 03:54:02', '2023-03-03 06:58:05'),
(5, 3, 'THAREQ DEZAN AHSAN', '', 'Rg==', '', 'Laki-laki', '$2y$10$I85B9ZrsVYKTztf0uX4PLOsVf8x7gq6Oa/hs287PgeVfmdeFi5cHe', 'students/graduated_-_Copy4.png', 1, '2023-03-06 03:54:16', '2023-03-03 07:01:18'),
(6, 4, 'MUHAMMAD BAGUS BURHAN', '', 'pQ==', '', 'Laki-laki', '$2y$10$TPpsLlqREQHOqWVnl7O3LemW46tBPuoihtS0LOG/iPIGg8/3wCYVG', 'students/graduated_-_Copy5.png', 1, '2023-03-06 03:54:26', '2023-03-03 07:02:58'),
(7, 4, 'FADILLAH RAHMANIA', '', 'CQ==', '', 'Perempuan', '$2y$10$mHu4HIKpmHYOzpMAoviCVe/WSSmrBpNJirda8blY/5gUMocZ2oAv.', 'students/graduated_-_Copy6.png', 1, '2023-03-06 03:54:36', '2023-03-03 07:04:40'),
(8, 6, 'Aqiilah Nur Zalfa', '', 'Fw==', '', 'Perempuan', '$2y$10$btIQTCZIB.vfXJtmlB8evO2Q7QlVCcNsAYlY1/7Mynh.rwQP62HAO', 'students/Female_Teacher6_(1)2.jpg', 1, '2023-03-27 02:07:17', '2023-03-03 07:06:05'),
(9, 6, 'Qurrota A\'yun Nugraha', '', 'nw==', '', 'Perempuan', '$2y$10$/53GScnxDRAhlrIQenuTOu3nw4HSaTkZV6sPNDDksvVbvuwzaFr3C', 'students/graduated_-_Copy11.png', 1, '2023-03-06 03:55:19', '2023-03-03 07:07:54'),
(10, 5, 'Tasya Alifia Febrianty', '', 'ug==', '', 'Perempuan', '$2y$10$.uXGtHKEZ386GLxMQSvKn.IwWrUZWWgLekWjqog6FVrWccbGpD5hO', 'students/Female_Teacher6_(1)1.jpg', 1, '2023-03-27 02:07:06', '2023-03-03 07:13:28'),
(11, 5, 'Titah Wahyu Fitria Anantya', '', 'HQ==', '', 'Perempuan', '$2y$10$ceBoXBr.W.3LXEXJIF6U3uo9ghb4Y2clpz7xu9Sfc6GICKMCefXIC', 'students/Female_Teacher6_(1).jpg', 1, '2023-03-27 02:06:50', '2023-03-03 07:14:59'),
(12, 3, 'gggg', '', 'rw==', '', 'Laki-laki', '$2y$10$Aw1q201KGz9v35WVBDV4belnltkFoeCpGre5YEAclhty15zRMtwdK', 'students/sandy-nugroho.png', 1, '2023-03-06 03:56:05', '2023-03-06 03:55:52'),
(13, 3, '564765765', '', 'mw==', '', 'Perempuan', '$2y$10$caNJ.T4QhfgJtu/sLZpvdO4xJoxGMifxQgNCWwAaT1d5k8dyPX.cy', 'students/graduated_-_Copy12.png', 1, NULL, '2023-03-06 04:02:03'),
(14, 4, 'rahma', '', 'a5fGBLps', '', 'Perempuan', '$2y$10$63SzpusVFKKwekv.M59YZOc9UehxTylnyfwmygcr/nxVnHhR0zQJK', 'students/cdbb36e2-af59-43fb-bfde-ba71f5287d8e.jpg', 1, '2023-03-07 02:35:12', '2023-03-07 02:30:23'),
(15, 3, 'Rahma', '', 'HC5a5d62', '', 'Laki-laki', '$2y$10$DNVVnA1/iWDpmqZOJsN8QuQM9Lkd/KLZ.M.Pc.VTr72Px0v3xCRwu', 'students/Female_Teacher6.jpg', 1, '2023-03-27 02:06:23', '2023-03-07 03:00:29'),
(16, 5, 'ioioio', '', 'mA==', '', 'Laki-laki', '$2y$10$byX4mzmX/LFWtHWRkQRhBeW/QNLhkD.AhR.83jmIlJcXNVEbKMQxq', 'students/', 1, NULL, '2023-03-23 07:24:50'),
(17, 5, 'tyui', '', 'Sg==', '', 'Laki-laki', '$2y$10$qVwMAj/R59M1I6agg3d1.O7z9h10EdxfxauZ/0CNk8fHBZxIhGrwC', 'students/Screenshot_(51).png', 1, '2023-03-23 07:26:18', '2023-03-23 07:26:08'),
(18, 3, '45654', '', 'tQ==', '', 'Laki-laki', '$2y$10$RrixzyMQeNu/XBhbXFELBOPo9Cx3EKJNMYvrdXCsI6wHI4e9XwBLu', 'students/employer.png', 1, NULL, '2023-03-24 08:31:41'),
(23, 2, 'Hanif Syafiul Huda', '20041098', '', 'e314a91d43', 'Laki-laki', '', 'students/WhatsApp_Image_2021-11-07_at_09_38_411.jpeg', 0, '2023-04-03 02:18:20', '2023-04-01 18:48:24'),
(24, 2, 'MOH. IQBAL SYAROF AL HAFIZ', '20041097', '', '90e5a020f5', 'Laki-laki', '', 'students/iqbl.jpg', 0, '2023-04-03 09:43:10', '2023-04-02 08:54:05'),
(25, 2, '534543', '', '', '', 'Laki-laki', '', 'students/Female_Teacher6_(1).jpg', 1, NULL, '2023-04-04 02:00:50'),
(26, 2, 'uy', '', '', '', 'Laki-laki', '', 'students/Female_Teacher6_(1).jpg', 1, '2023-04-04 02:03:53', '2023-04-04 02:03:13'),
(27, 0, 'Putra', '', '', '', 'Laki-laki', '', '', 1, NULL, NULL),
(28, 0, 'Putri', '', '', '', 'Perempuan', '', '', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `student_attandances`
--

CREATE TABLE `student_attandances` (
  `id_student_attandance` int(11) NOT NULL,
  `id_device` int(11) NOT NULL,
  `id_student` int(11) NOT NULL,
  `masuk` int(11) NOT NULL,
  `waktu_masuk` varchar(50) NOT NULL,
  `keluar` int(11) DEFAULT NULL,
  `waktu_keluar` varchar(50) DEFAULT NULL,
  `status_hadir` varchar(11) NOT NULL,
  `ket` varchar(100) NOT NULL,
  `edited_by` varchar(100) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL,
  `is_updated` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id_user` int(11) NOT NULL,
  `id_role` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `foto` varchar(100) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id_user`, `id_role`, `nama`, `foto`, `username`, `password`, `created_at`, `updated_at`, `deleted`) VALUES
(1, 1, 'Administrator', 'users/graduated_-_Copy.png', 'admin', '$2y$10$h2uwSZch946wMiBjYjICVeZLxaHZTPllPiHB6f0ApnBsAiuSWAT1C', NULL, '2023-03-06 03:58:35', 0),
(2, 2, 'Admin Topup', 'users/graduated_-_Copy3.png', 'topup', '$2y$10$QQpsvEe8NjVEq/6c6dNZhu/j91igbtkP3Q9u60EbPR8GUiaDu3JeC', '2022-07-31 12:01:18', '2023-03-06 04:00:55', 0),
(3, 1, 'Admin', 'users/graduated_-_Copy4.png', 'administrator', '$2a$12$uYMT8ZfTP25x6b6ZuwjyoeWCUHJYYCfmKPy5zV4JZH9TrItUWWHxW', NULL, '2023-03-06 04:01:03', 1),
(4, 3, 'Admin Absensi', 'users/graduated_-_Copy5.png', 'absensi', '$2a$12$yS4RLWB.qfNiwJnowDa06.NZB6ntXy6woRXIsMJqG5sXw8lbxPh8S', NULL, '2023-03-06 04:01:12', 0),
(8, 4, 'Viewer', 'users/graduated_-_Copy2.png', 'viewer', '$2y$10$Dy2xAk0PLwFJQypHpwgFu.om1.ISDlzPmeUwKsKlW94ChGgBa5Pda', '2023-02-15 07:23:01', '2023-03-06 04:00:48', 0);

-- --------------------------------------------------------

--
-- Table structure for table `weekly_holidays`
--

CREATE TABLE `weekly_holidays` (
  `id_weekly_holiday` int(11) NOT NULL,
  `hari` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `attandance_devices`
--
ALTER TABLE `attandance_devices`
  ADD PRIMARY KEY (`id_attandance_device`);

--
-- Indexes for table `attendances`
--
ALTER TABLE `attendances`
  ADD PRIMARY KEY (`id_attendance`);

--
-- Indexes for table `classes`
--
ALTER TABLE `classes`
  ADD PRIMARY KEY (`id_class`);

--
-- Indexes for table `holidays`
--
ALTER TABLE `holidays`
  ADD PRIMARY KEY (`id_holiday`);

--
-- Indexes for table `operational_times`
--
ALTER TABLE `operational_times`
  ADD PRIMARY KEY (`id_operational_time`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id_role`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id_setting`);

--
-- Indexes for table `students`
--
ALTER TABLE `students`
  ADD PRIMARY KEY (`id_student`);

--
-- Indexes for table `student_attandances`
--
ALTER TABLE `student_attandances`
  ADD PRIMARY KEY (`id_student_attandance`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id_user`);

--
-- Indexes for table `weekly_holidays`
--
ALTER TABLE `weekly_holidays`
  ADD PRIMARY KEY (`id_weekly_holiday`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `attandance_devices`
--
ALTER TABLE `attandance_devices`
  MODIFY `id_attandance_device` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `attendances`
--
ALTER TABLE `attendances`
  MODIFY `id_attendance` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `classes`
--
ALTER TABLE `classes`
  MODIFY `id_class` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `holidays`
--
ALTER TABLE `holidays`
  MODIFY `id_holiday` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `operational_times`
--
ALTER TABLE `operational_times`
  MODIFY `id_operational_time` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id_role` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id_setting` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `students`
--
ALTER TABLE `students`
  MODIFY `id_student` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `student_attandances`
--
ALTER TABLE `student_attandances`
  MODIFY `id_student_attandance` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `weekly_holidays`
--
ALTER TABLE `weekly_holidays`
  MODIFY `id_weekly_holiday` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
